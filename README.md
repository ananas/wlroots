# wlroots

For just about everything, [see upstream repository](https://gitlab.freedesktop.org/wlroots/wlroots/).

This is a custom branch that might or might not be up to date, you probably should **not** use this.
It is made for fixing stuff related to my hobby compositor.


## Differences to the upstream branch (patches avaiable in /patches)


